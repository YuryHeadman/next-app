import parse from 'html-react-parser'
import qs from 'qs'
import { apiUrl } from '@configs/api_config'
// export const apiUrl = 'http://localhost:1337/api'

export const fetchApi = async (path, urlParamsObject = {}, options = {}) => {
    try {
        const mergedOption = {
            headers: {
                'Content-Type': 'application/json',
            },
            ...options,
        }

        const _urlParamsObject = {
            ...urlParamsObject,
            'pagination[limit]': '-1', // get all entries
        }

        const queryString = qs.stringify(_urlParamsObject)

        const requestUrl = `${apiUrl}/api${path}${
            queryString ? `?${queryString}` : ''
        }`

        const response = await fetch(requestUrl, mergedOption)

        return await response.json()
    } catch (error) {
        console.error(error.message)
    }
}

export const parseText = (text) =>
    text && parse(text.replace(/\/uploads\//g, `${apiUrl}/uploads/`))
