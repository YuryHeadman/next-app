import { useState, useEffect } from 'react'
import Link from 'next/link'
import styles from './styles.module.scss'
import classNames from 'classnames'
import { Article, Tag } from '@/types'

const textLength = 40

type FilterButtonProps = React.HTMLAttributes<HTMLButtonElement> & {
    text: string
}

const FilterButton = ({ text, ...props }: FilterButtonProps) => (
    <button {...props}>{text}</button>
)

type BlogPostsProps = {
    articles: Article[]
    tags: Tag[]
}

const BlogPosts = ({ articles, tags }: BlogPostsProps) => {
    const [filterTags, setFilterTags] = useState<string[]>([])
    const handleClick = (evt: React.MouseEvent<HTMLButtonElement>) => {
        const tagname = evt.currentTarget.dataset.name || 'all'
        if (tagname === 'all') {
            setFilterTags([])
        } else if (filterTags.includes(tagname)) {
            setFilterTags(filterTags.filter((tag) => tag !== tagname))
        } else {
            setFilterTags([...filterTags, tagname])
        }
    }

    const filteredArticles =
        filterTags.length === 0
            ? articles
            : articles.filter((article) => {
                  const postTags = article.attributes?.tags_new?.data.map(
                      (tag) => tag.attributes.tagname
                  )
                  return filterTags.some((tag) => postTags.includes(tag))
              })

    return (
        <>
            <nav>
                <div className={styles.blogLinks}>
                    <FilterButton
                        onClick={handleClick}
                        className={classNames(styles.blogLink, {
                            [styles.active]: filterTags?.includes('all'),
                        })}
                        data-name={'all'}
                        text={'All'}
                    />
                    {tags.map((tag) => (
                        <FilterButton
                            key={tag.attributes.tagname}
                            onClick={handleClick}
                            className={classNames(styles.blogLink, {
                                [styles.active]:
                                    filterTags?.includes(
                                        tag.attributes.tagname
                                    ) || filterTags.length === 0,
                            })}
                            data-name={tag.attributes.tagname}
                            text={tag.attributes.title}
                        />
                    ))}
                </div>
            </nav>
            <h1>Blog page</h1>
            <div className={styles.articles}>
                {filteredArticles?.map(({ id, attributes }: Article) => (
                    <div key={id} className={styles.article}>
                        <h3>{attributes.title}</h3>
                        <p className={styles.articleText}>
                            {attributes.text.slice(0, textLength)}
                            {attributes.text.length > textLength && '...'}
                        </p>
                        <div className={styles.tags}>
                            {attributes.tags_new?.data.map((item) => (
                                <span
                                    key={item.attributes.tagname}
                                    className={styles.tag}
                                >
                                    {item.attributes.tagname}
                                </span>
                            ))}
                        </div>
                        <span className={styles.articleDate}>
                            {new Date(attributes.publishedAt).toLocaleString()}
                        </span>
                        <Link
                            className={styles.articleLink}
                            href={`/blog/${attributes.slug}`}
                        >
                            See more
                        </Link>
                    </div>
                ))}
                {!articles && <p>Sorry, some problem with fetch </p>}
            </div>
        </>
    )
}

export default BlogPosts
