import Link from 'next/link'
import styles from './page.module.scss'
import RootLayout from '@/components/layout'

import '@pages/globals.css'
import { Metadata } from 'next'
import Head from 'next/head'
import AppNav from './AppNav'
import Footer from './Footer'

const Layout = ({
    children,
    metatags,
}: {
    children: React.ReactNode
    metatags: any
}) => {
    return (
        <main className={styles.main}>
            <Head>
                <meta name="description" content={metatags.description} />
                <title>{metatags.title}</title>
            </Head>
            <AppNav />
            {children}
            <Footer />
        </main>
    )
}

export default Layout
