import { navLinks } from '@/configs/configs'
import styles from './styles.module.scss'
import Link from 'next/link'

const Index = () => {
    return (
        <div className={styles.grid}>
            {navLinks.map(({ title, path }) => (
                <Link href={path} key={title} className={styles.card}>
                    <span>
                        {title} <span>-&gt;</span>
                    </span>
                </Link>
            ))}
        </div>
    )
}

export default Index
