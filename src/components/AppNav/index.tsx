import Link from 'next/link'
import styles from './styles.module.scss'
import { NavLink } from '@/types'
import classNames from 'classnames'
import { useRouter } from 'next/router'
import { navLinks } from '@/configs/configs'

const AppNav = () => {
    const router = useRouter()

    return (
        <header className={styles.nav}>
            {navLinks.map(({ title, path }) => (
                <Link
                    key={title}
                    href={path}
                    className={classNames(styles.navLink, {
                        [styles.active]: router.pathname === path,
                    })}
                >
                    {title}
                </Link>
            ))}
        </header>
    )
}

export default AppNav
