import { NavLink } from '@/types'

export const navLinks: NavLink[] = [
    { title: 'Home', path: '/' },
    { title: 'Blog', path: '/blog' },
]
