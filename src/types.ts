export type Article = {
    id: number
    attributes: {
        title: string
        text: string
        slug: string
        tags: string
        createdAt: string
        updatedAt: string
        publishedAt: string
        locale: string
        image: ArticleImage
        tags_new: { data: Tag[] }
        beautiful_text: string
        localizations: { data: any }
    }
}
// TODO localizations типизировать 👆🏻

export type Tag = {
    id: number
    attributes: {
        tagname: string
        title: string
        createdAt: string
        updatedAt: string
        publishedAt: string
    }
}

type ArticleImage = {
    data: {
        id: number
        attributes: {
            name: string
            alternativeText: string | null
            caption: string | null
            width: number
            height: number
            formats: {
                large?: {
                    ext: string
                    url: string
                    hash: string
                    mime: string
                    name: string
                    path: string | null
                    size: number
                    width: number
                    height: number
                }
                small: {
                    ext: string
                    url: string
                    hash: string
                    mime: string
                    name: string
                    path: string | null
                    size: number
                    width: number
                    height: number
                }
                medium?: {
                    ext: string
                    url: string
                    hash: string
                    mime: string
                    name: string
                    path: string | null
                    size: number
                    width: number
                    height: number
                }
                thumbnail: {
                    ext: string
                    url: string
                    hash: string
                    mime: string
                    name: string
                    path: string | null
                    size: number
                    width: number
                    height: number
                }
            }
            hash: string
            ext: string
            mime: string
            size: number
            url: string
            previewUrl: string | null
            provider: string
            provider_metadata: string | null
            createdAt: string
            updatedAt: string
        }
    } | null
}

export type NavLink = {
    title: string
    path: string
}
