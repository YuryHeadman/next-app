import Head from 'next/head'
import React from 'react'

const MyApp = ({ Component, pageProps }) => {
    return (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
            </Head>

            <Component {...pageProps} />
        </>
    )
}

export default MyApp
