import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'
// import { Inter } from 'next/font/google'
// const inter = Inter({ subsets: ['latin'] })

export default class MyDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <link rel="icon" href="/favicon.ico" />
                </Head>

                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with server-side generation (SSG).
MyDocument.getInitialProps = async (ctx) => {
    const initialProps = await Document.getInitialProps(ctx)

    return { ...initialProps }
}
