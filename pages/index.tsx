// import Image from 'next/image'
import RootLayout from '@/components/layout'
import styles from './page.module.scss'

export const metatags = {
    title: 'Create Next App',
    description: 'Generated by create next app',
}

const Index = () => {
    return (
        <RootLayout metatags={metatags}>
            <div className={styles.center}>
                {/* <Image
                className={styles.logo}
                src="/next.svg"
                alt="Next.js Logo"
                width={180}
                height={37}
                priority
            /> */}
                <h1>Main page</h1>
            </div>
        </RootLayout>
    )
}

export default Index
